﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TMG_F.UnityUtilities {
    public class Test : MonoBehaviour {

        PointerManager pointerManager;

        void Awake() {
            pointerManager = GetComponent<PointerManager>();

            pointerManager.AddEventListener(PointerManager.EventType.Down, (Vector2 position, Vector2 deltaPosition) => {
                Debug.Log("down:" + position);
            });
            pointerManager.AddEventListener(PointerManager.EventType.Up, (Vector2 position, Vector2 deltaPosition) => {
                Debug.Log("up:" + position);
            });
            pointerManager.AddEventListener(PointerManager.EventType.DownMove, (Vector2 position, Vector2 deltaPosition) => {
                Debug.Log("down move:" + position + "," + deltaPosition);
            });
            pointerManager.AddEventListener(PointerManager.EventType.Tap, (Vector2 position, Vector2 deltaPosition) => {
                Debug.Log("tap(click):" + position + "," + deltaPosition);
            });
            pointerManager.AddEventListener(PointerManager.EventType.DoubleTap, (Vector2 position, Vector2 deltaPosition) => {
                Debug.Log("double tap(double click):" + position + "," + deltaPosition);
            });
        }
    }
}
