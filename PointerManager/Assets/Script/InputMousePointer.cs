﻿using UnityEngine;
using System.Collections;

namespace TMG_F.UnityUtilities {

    [RequireComponent(typeof(PointerManager))]
    public class InputMousePointer : MonoBehaviour {

        public bool isValid = true;
        PointerManager inputManager;

        // Use this for initialization
        void Awake(){
            inputManager = GetComponent<PointerManager>();
        }

        // Update is called once per frame
        void Update() {
            if (!isValid)
                return;

            if (Input.GetMouseButtonDown(0)) {
                inputManager.PointerDown(0, Input.mousePosition);
            }
            if (Input.GetMouseButtonUp(0)) {
                inputManager.PointerUp(0, Input.mousePosition);
            }
            if (Input.GetMouseButton(0)) {
                inputManager.PointerDown(0, Input.mousePosition);
            }
        }
    }

}
