﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

namespace TMG_F.UnityUtilities{
	public class PointerManager : MonoBehaviour {

        //public event
        public delegate void OnPointerEventHanlder (Vector2 position, Vector2 deltaPosition);

		//public param
		public float MoveMargin = 3f;
		public float LongPressTime = 1f;
		public float TapTime = 0.5f;
		public float FlickFrameSpeed = 10f;
		public float DoubleTapMargin = 0.5f;

		//private param
		Vector2[] startPosition = new Vector2[2];
		Vector2[] prevPosition = new Vector2[2];
		Vector2[] deltaPosition = new Vector2[2];
		float[] downStartTime = new float[2];
		bool[] isDowned = new bool[2]{false,false};
		bool[] isMoved = new bool[2]{false,false};
		bool[] isLongPressed = new bool[2]{false,false};
		int downCount = 0;
		float firstTapTime;
		bool isFirstTaped = false;

        //single finger event
        List<OnPointerEventHanlder> downEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder DownEvent;
        List<OnPointerEventHanlder> tapEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder TapEvent;
        List<OnPointerEventHanlder> doubleTapEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder DoubleTapEvent;
        List<OnPointerEventHanlder> upEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder UpEvent;
        List<OnPointerEventHanlder> flickEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder FlickEvent;
        List<OnPointerEventHanlder> downMoveEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder DownMoveEvent;
        List<OnPointerEventHanlder> longDownEventHandlerList = new List<OnPointerEventHanlder>();
        event OnPointerEventHanlder LongDownEvent;
        List<OnPointerEventHanlder> keepDownEventHandlerList = new List<OnPointerEventHanlder>();
		event OnPointerEventHanlder KeepDownEvent;

		//double finger event
//		public delegate void PinchInEventHandler(Vector2 vec);
//		public event PinchInEventHandler PinchInEvent;
//		public delegate void PinchOutEventHandler(Vector2 vec);
//		public event PinchOutEventHandler PinchOutEvent;

        public enum EventType {
            Down,Tap,DoubleTap,Up,Flick,DownMove,LongDown,KeepDown
        }

		public void PointerDown(int index, Vector2 position){
			//single down
			if(index == 0){
				if(!isDowned[0]){
					SingleDown(position);
				}
			}
			//down start
			if(!isDowned[index]){
				downStartTime[index] = Time.time;
				startPosition[index] = position;
				downCount++;
				isDowned[index] = true;
			} else {
				//keep down
				SingleKeepDown(position);
			}
			//move
			Move (index, position);
			//long press
			SingleLongPress(position);

			prevPosition[index] = position;
		}

		public void PointerUp(int index, Vector2 position){
			downCount--;
			if(index == 0 && downCount == 0){
				//tap
				SingleTap (position);
				//flick
				SingleFlick(deltaPosition[index]);
			}

			isDowned[index] = false;
			isLongPressed[index] = false;
			isMoved[index] = false;
		}

        public void AddEventListener(EventType eventType, OnPointerEventHanlder handler) {
            switch (eventType) {
                case EventType.DoubleTap:
                    doubleTapEventHandlerList.Add(handler);
                    DoubleTapEvent += handler;
                    break;
                case EventType.Down:
                    downEventHandlerList.Add(handler);
                    DownEvent += handler;
                    break;
                case EventType.DownMove:
                    downMoveEventHandlerList.Add(handler);
                    DownMoveEvent += handler;
                    break;
                case EventType.Flick:
                    flickEventHandlerList.Add(handler);
                    FlickEvent += handler;
                    break;
                case EventType.KeepDown:
                    keepDownEventHandlerList.Add(handler);
                    KeepDownEvent += handler;
                    break;
                case EventType.LongDown:
                    longDownEventHandlerList.Add(handler);
                    LongDownEvent += handler;
                    break;
                case EventType.Tap:
                    tapEventHandlerList.Add(handler);
                    TapEvent += handler;
                    break;
                case EventType.Up:
                    upEventHandlerList.Add(handler);
                    UpEvent += handler;
                    break;
                default:
                    Debug.LogWarning("invalid event type:" + eventType);
                    break;
            }
        }

        public void RemoveEventLisetener(EventType eventType, OnPointerEventHanlder handler) {
            switch (eventType) {
                case EventType.DoubleTap:
                    doubleTapEventHandlerList.Remove(handler);
                    DoubleTapEvent -= handler;
                    break;
                case EventType.Down:
                    downEventHandlerList.Remove(handler);
                    DownEvent -= handler;
                    break;
                case EventType.DownMove:
                    downMoveEventHandlerList.Remove(handler);
                    DownMoveEvent -= handler;
                    break;
                case EventType.Flick:
                    flickEventHandlerList.Remove(handler);
                    FlickEvent -= handler;
                    break;
                case EventType.KeepDown:
                    keepDownEventHandlerList.Remove(handler);
                    KeepDownEvent -= handler;
                    break;
                case EventType.LongDown:
                    longDownEventHandlerList.Remove(handler);
                    LongDownEvent -= handler;
                    break;
                case EventType.Tap:
                    tapEventHandlerList.Remove(handler);
                    TapEvent -= handler;
                    break;
                case EventType.Up:
                    upEventHandlerList.Remove(handler);
                    UpEvent -= handler;
                    break;
                default:
                    Debug.LogWarning("invalid event type:" + eventType);
                    break;
            }
        }

        public void ClearEventListener(EventType eventType) {
            switch (eventType) {
                case EventType.DoubleTap:
                    doubleTapEventHandlerList.ForEach((handler) => {
                        DoubleTapEvent -= handler;
                    });
                    doubleTapEventHandlerList.Clear();
                    break;
                case EventType.Down:
                    downEventHandlerList.ForEach((handler) => {
                        DownEvent -= handler;
                    });
                    downEventHandlerList.Clear();
                    break;
                case EventType.DownMove:
                    downMoveEventHandlerList.ForEach((handler) => {
                        DownMoveEvent -= handler;
                    });
                    downMoveEventHandlerList.Clear();
                    break;
                case EventType.Flick:
                    flickEventHandlerList.ForEach((handler) => {
                        FlickEvent -= handler;
                    });
                    flickEventHandlerList.Clear();
                    break;
                case EventType.KeepDown:
                    keepDownEventHandlerList.ForEach((handler) => {
                        KeepDownEvent -= handler;
                    });
                    keepDownEventHandlerList.Clear();
                    break;
                case EventType.LongDown:
                    longDownEventHandlerList.ForEach((handler) => {
                        LongDownEvent -= handler;
                    });
                    longDownEventHandlerList.Clear();
                    break;
                case EventType.Tap:
                    tapEventHandlerList.ForEach((handler) => {
                        TapEvent -= handler;
                    });
                    tapEventHandlerList.Clear();
                    break;
                case EventType.Up:
                    upEventHandlerList.ForEach((handler) => {
                        UpEvent -= handler;
                    });
                    upEventHandlerList.Clear();
                    break;
                default:
                    Debug.LogWarning("invalid event type:" + eventType);
                    break;
            }
        }

        public void ClearAllEventListener() {
            foreach(EventType eventType in Enum.GetValues(typeof(EventType))){
                ClearEventListener(eventType);
            }
        }

		void SingleDown(Vector2 position){
			if(DownEvent != null){
				DownEvent(position, Vector2.zero);
			}
		}

		void SingleUp(Vector2 position){
			if(UpEvent != null){
				UpEvent(position, Vector2.zero);
			}
		}

		void SingleTap(Vector2 position){
			SingleUp(position);
			var diffTime = Time.time - downStartTime[0];
			if(!isMoved[0] && diffTime < TapTime){
				//double tap
				if(isFirstTaped){
					var tapDiffTime = Time.time - firstTapTime;
					if(tapDiffTime < DoubleTapMargin){
						if(DoubleTapEvent != null){
							DoubleTapEvent(position, Vector2.zero);
							return;
						}
					}
					isFirstTaped = false;
				}
				//single tap
				if(!isFirstTaped){
					isFirstTaped = true;
					firstTapTime = Time.time;
					if(TapEvent != null){
						TapEvent(position, Vector2.zero);
					}
				}
			}
		}

		void SingleLongPress(Vector2 position){
			if(!isLongPressed[0] && !isMoved[0]){
				var diffTime = Time.time - downStartTime[0];
				if(diffTime > LongPressTime){
					isLongPressed[0] = true;
					if(LongDownEvent != null){
						LongDownEvent(position, Vector2.zero);
					}
				}
			}
		}

		void Move(int index, Vector2 position){
			var sqrDiffDistance = (position - startPosition[index]).sqrMagnitude;
			if(sqrDiffDistance > MoveMargin * MoveMargin){
				isMoved[index] = true;
			}
			if(isMoved[index]){
				deltaPosition[index] = position - prevPosition[index];
				if(deltaPosition[index].sqrMagnitude > 0){
					if(index == 0){
						if(DownMoveEvent != null){
							DownMoveEvent(position, deltaPosition[index]);
						}
					}
				}
			}
		}

		void SingleFlick(Vector2 speedVector){
			var sqrSpeed = speedVector.sqrMagnitude;
			if(sqrSpeed > FlickFrameSpeed){
				if(FlickEvent != null){
					FlickEvent(speedVector.normalized, Vector2.zero);
				}
			}
		}

		void SingleKeepDown(Vector2 position){
			if(KeepDownEvent != null){
				KeepDownEvent(position, Vector2.zero);
			}
		}
	}

}

